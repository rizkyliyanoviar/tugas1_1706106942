import java.util.Scanner;
public class Soal1 {
    public static void main (String args []){

        Scanner gaji = new Scanner(System.in);
		
        boolean y = false;
        int jumlahBulan = 0;
        while (y == false) { //Perulangan untuk memasukkan input bulan
            System.out.print("Masukkan Bulan (1-12): ");
            jumlahBulan = gaji.nextInt();
            if (jumlahBulan < 1 || jumlahBulan > 12) { 
                System.out.println("Kode bulan yang dimasukkan tidak valid");
            } else {
                y = true;
            }
        }
		//Varible menampung jumlah pegawai
        int jmlManager=0;
        int jmlStaf=0;
        int jmlHonorer=0;
		//Varible menampung gaji manager
        double gajiManagerTotal=0;
		//Variable menampung gaji staf
        double gajiStaf=0;
        double stafLembur=0;
        double stafThr=0;
        double gajiStafTotal=0;
		//Variable menampung gaji honorer
        double gajiHonorerTotal=0;
		//Variable menampung semua gaji pegawai
        double gajiTotalPegawai=0;
		//Variable menampung x untuk perulangan
        boolean x = false;

        while (x == false) { //Perulangan untuk input jenis pegawai
            System.out.print("Masukkan Jenis Pegawai (M, S, H) (X untuk selesai): ");
            String kodePegawai = gaji.next();
            if (kodePegawai.equals("M")) {
                jmlManager += 1; //Jika manager yang diinput lebih dari 1 maka akan ditampung
                System.out.println("Jenis Pegawai Manager!");
                System.out.print("Masukkan gajinya (juta): ");
                double gajiManager = gaji.nextDouble();
                if (jumlahBulan == 12) { //Perulangan menghitung THR
                    gajiManagerTotal += (gajiManager * 2);
                } else {
                    gajiManagerTotal += gajiManager;
                }
            } else if (kodePegawai.equals("S")) {
                jmlStaf += 1; //Jika staf yang diinput lebih dari 1 maka akan ditampung
                System.out.println("Jenis Pegawai Staf!");
                System.out.print("Masukkan masa kerjanya (tahun): ");
                int masaKerja = gaji.nextInt();
                System.out.print("Jumlah jam lembur: ");
                int jamLembur = gaji.nextInt();
                if (jumlahBulan == 12) { //Perulangan menghitung THR dan lembur
                    if (masaKerja <= 3) {
                        gajiStaf = 1.5;
                    } else if (masaKerja <= 5) {
                        gajiStaf = 2;
                    } else if (masaKerja > 5) {
                        gajiStaf = 1.5 + 0.2 * masaKerja;
                    }
                    stafLembur = 0.125 * jamLembur;
                    stafThr = 0.75 * gajiStaf;
                    gajiStafTotal = gajiStaf + stafLembur + stafThr;
                } else {
                    if (masaKerja <= 3) {
                        gajiStaf = 1.5;
                    } else if (masaKerja <= 5) {
                        gajiStaf = 2;
                    } else if (masaKerja > 5) {
                        gajiStaf = 1.5 + 0.2 * masaKerja;
                    }
                    stafLembur = 0.125 * jamLembur;
                    gajiStafTotal = gajiStaf + stafLembur;
                }
            } else if (kodePegawai.equals("H")) {
                jmlHonorer += 1; //Jika pegawai honorer yang diinput lebih dari 1 maka akan ditampung
                System.out.println("Jenis Pegawai Honorer!");
                System.out.print("Jumlah jam: ");
                int jumlahJam = gaji.nextInt();
                gajiHonorerTotal = 0.1 * jumlahJam; //Perhitungan gaji honorer
            } else if (kodePegawai.equals("X")) { //Jika memilih X maka akan langsung ditotal semua gaji pegawai
                x = true;
                System.out.println("********************************************************");
            } else { //Jika yang diinput selain M, S, H, dan X maka akan salah
                System.out.println("Kode pegawai yang dimasukkan tidak valid!");
            }
            gajiTotalPegawai = gajiManagerTotal + gajiStafTotal + gajiHonorerTotal;
        }
        System.out.println("Jumlah Manager: "+jmlManager+", Total Gaji: "+gajiManagerTotal+" juta");
        System.out.println("Jumlah Staf: "+jmlStaf+", Total Gaji: "+gajiStafTotal+" juta");
        System.out.println("Jumlah Honorer: "+jmlHonorer+", Total Gaji: "+gajiHonorerTotal+" juta");
        System.out.println("TOTAL YANG HARUS DIBAYARKAN PT. ABC: "+gajiTotalPegawai+" juta");
        System.out.println("********************************************************");
    }
}
