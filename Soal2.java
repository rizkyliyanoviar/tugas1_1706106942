import java.util.Scanner;
public class Soal2 {
    public void cetak (int n){
        int baris, kolom; //Variable untuk baris dan kolom
        if (n % 2 == 0){ 
            for (baris = 1; baris <= 4; baris++){ //Menampilkan baris sebanyak 4 bintang
                for (kolom = 1; kolom <= baris; kolom++){
                    System.out.print("*");
                }
                for (kolom = 1; kolom <= n-2; kolom++){ //Menampilkan kolom sebanyak n-2
                    System.out.print(" ");
                }
                for(kolom = 1; kolom <= baris; kolom++){
                    System.out.print("*");
                }
                System.out.println();
                n = n-2;
            }
        }
        else {
            n--;
            for (baris = 1; baris <= 4; baris++){ //Menampilkan baris sebanyak 4 bintang
                for (kolom = 1; kolom <= baris; kolom++){
                    System.out.print("*");
                }
                for (kolom = 1; kolom <= n-1; kolom++){ //Menampilkan kolom sebanyak n-1
                    System.out.print(" ");
                }
                for(kolom = 1; kolom <= baris; kolom++){
                    System.out.print("*");
                }
                System.out.println();
                n = n-2;
            }
        }
    }
    public static void main (String args []){
        Soal2 bintang = new Soal2();
        Scanner obj= new Scanner(System.in);
        System.out.print("Masukkan nilai n (1-19) : ");
        int star=obj.nextInt();
        if (star >= 1 && star <= 19){ //Jika bintang lebih dari sama dengan 1 dan lebih kecil dari sama dengan 19
            bintang.cetak(star);
        }
        else {
            System.out.println("Nilai n yang dimasukkan tidak valid!");
        }
    }
}
